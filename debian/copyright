Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: KeePass2
Upstream-Contact: Dominik Reichl <dominik.reichl@t-online.de>
Source: http://keepass.info/ repackaged to remove icon files obtained from other password managers with non dfsg free licenses, listing in debian/README.source
Files-Excluded: KeePass/Resources/Images/B16x16_Imp_Enpass.png KeePass/Resources/Images/B16x16_Imp_MSecure.png KeePass/Resources/Images/B16x16_Imp_PwSaver.png KeePass/Resources/Images/B16x16_Imp_PwDepot.png KeePass/Resources/Images/B16x16_Imp_TurboPws.png KeePass/Resources/Images/B16x16_Imp_PassKeeper.png KeePass/Resources/Images/B16x16_Imp_HandySafePro.png KeePass/Resources/Images/B16x16_Imp_LastPass.png KeePass/Resources/Images/B16x16_Imp_PINs.png KeePass/Resources/Images/B16x16_Imp_DataVault.png KeePass/Resources/Images/B16x16_Imp_Whisper32.png KeePass/Resources/Images/B16x16_Imp_AmP.png KeePass/Resources/Images/B16x16_Imp_NetworkPwMgr.png KeePass/Resources/Images/B16x16_Imp_StickyPw.png KeePass/Resources/Images/B16x16_Imp_PwTresor.png KeePass/Resources/Images/B16x16_Imp_SplashID.png KeePass/Resources/Images/B16x16_Imp_DesktopKnox.png KeePass/Resources/Images/B16x16_Imp_PwSafe.png KeePass/Resources/Images/B16x16_Imp_KasperskyPwMgr.png KeePass/Resources/Images/B16x16_Imp_NortonIdSafe.png KeePass/Resources/Images/B16x16_Imp_PwAgent.png KeePass/Resources/Images/B16x16_Imp_PwsPlus.png KeePass/Resources/Images/B16x16_Imp_Dashlane.png KeePass/Resources/Images/B16x16_Imp_NPassword.png KeePass/Resources/Images/B16x16_Imp_HandySafe.png KeePass/Resources/Images/B16x16_Imp_OnePwPro.png KeePass/Resources/Images/B16x16_Imp_Revelation.png KeePass/Resources/Images/B16x16_Imp_RoboForm.png KeePass/Resources/Images/B16x16_Imp_AnyPw.png KeePass/Resources/Images/B16x16_Imp_PwPrompter.png KeePass/Resources/Images/B16x16_Imp_Steganos.png KeePass/Resources/Images/B16x16_Imp_PVault.png KeePass/Resources/Images/B16x16_Imp_CWallet.png KeePass/Resources/Images/B16x16_Imp_Security.png KeePass/Resources/Images/B16x16_Imp_SafeWallet.png KeePass/Resources/Images/B16x16_Imp_PwMem2008.png KeePass/Resources/Images/B16x16_Imp_ZdnPwPro.png KeePass/Resources/Images/B16x16_Imp_VisKeeper.png KeePass/Resources/Images/B16x16_Imp_FlexWallet.png KeePass/Resources/Images/B16x16_Imp_Bitwarden.png KeePass/Resources/Images/B16x16_Imp_Steganos20.png Build/KeePassLib_Distrib/KeePassLib.dll

Files: *
Copyright: 2003-2012 Dominik Reichl
License: GPL-2+

Files: debian/*
Copyright: 2011, 2012, Julian Taylor
License: GPL-2+

Files: KeePass/Resources/Nuvola/* KeePass/Resources/Nuvola/* Translation/TrlUtil/Resources/* Ext/Images_App_HighRes/*
Copyright: 2003, 2004, David Vignoni
License: LGPL-2.1withAddon

Files: KeePass/Resources/Nuvola_Derived/*
Copyright: 2003, 2004, David Vignoni
           2011, Dominik Reichl
License: LGPL-2.1withAddon

Files: KeePass/Resources/Images/B16x16_Imp_KeePassX.png
Copyright: 2005-2008, Tarek Saidi
           2007-2009, Felix Geyer
License: GPL-2+

Files: KeePass/Resources/Icons/*  KeePass/KeePass.ico KeePass/Resources/Images/* Translation/TrlUtil/Resources/KeePass.ico
Copyright: 2011, Christopher Bolin
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1withAddon
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation,
 version 2.1 of the License.
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 You should have received a copy of the GNU Lesser General Public
 License along with this library (see the the license.txt file); if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
 .
 #######**** NOTE THIS ADD-ON ****#######
 The GNU Lesser General Public License or LGPL is written for software libraries
 in the first place. The LGPL has to be considered valid for this artwork
 library too.
 Nuvola icon theme for KDE 3.x is a special kind of software library, it is an
 artwork library, it's elements can be used in a Graphical User Interface, or
 GUI.
 Source code, for this library means:
 - raster png image* .
 The LGPL in some sections obliges you to make the files carry
 notices. With images this is in some cases impossible or hardly usefull.
 With this library a notice is placed at a prominent place in the directory
 containing the elements. You may follow this practice.
 The exception in section 6 of the GNU Lesser General Public License covers
 the use of elements of this art library in a GUI.
 dave [at] icon-king.com
